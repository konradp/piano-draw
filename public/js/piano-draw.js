class PianoDraw {
  constructor(canvas_id, settings=null) {
    this.state = {
      '0': {
        path: null,
        color: null,
      },
      // ...
    };
    this.canvas = document.getElementById(canvas_id);
    this.c = this.canvas.getContext('2d');
    this.countOctaves = 3;
    if (settings != null) {
      if (settings.countOctaves) {
        this.countOctaves = settings.countOctaves;
      }
    }
    //this.countOctaves = (this.settings.countOctaves != undefined)? this.settings.countOctaves : 3;
    this.countKeys = this.countOctaves*12;
    this.countWhites = this.countOctaves*7;
    this.keyMap = [ 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0 ];
    this.margin = this.canvas.width/20;
    this.boxW = this.canvas.width - 2*this.margin;
    this.boxH = this.canvas.height - 2*this.margin;
    this.whiteW = this.boxW/this.countWhites;
    this.blackWidthMultiplier = 3/5;
    this.blackW = this.whiteW*this.blackWidthMultiplier;
    this.drawKeyboard();
    this.canvas.addEventListener('mousedown', this.onMouseDown.bind(this));
  };


  setKeys(keys) {
    // keys to be pressed
    // keys = [ 1, 3, 4, 6, 8 ];
    if (keys.length == 0) {
      this.state = {
        path: null,
        color: null,
      };
      this.drawKeyboard();
      return;
    }
    for (let key of keys) {
      if (Number.isInteger(key)) {
        this.state[key].isDown = true;
      } else {
        // An object, likely including a colour
        if (key.hasOwnProperty('key')) {
          this.state[key.key].isDown = true;
        }
        if (key.hasOwnProperty('color')) {
          this.state[key.key].color = key.color;
        }
      }
    }
    this.drawKeyboard();
  }

  getKeys() {
    // TODO
  }


  noteOn(key, color=null) {
    this.state[key].isDown = true;
    if (color != null) {
      this.state[key].color = color;
    }
    this.drawKeyboard();
  }


  noteOff(key, color=null) {
    this.state[key].isDown = false;
    if (color != null) {
      this.state[key].color = color;
    }
    this.drawKeyboard();
  }


  noteFlip(key, color=null) {
    this.state[key].isDown = !this.state[key].isDown;
    if (color != null) {
      this.state[key].color = color;
    }
    this.drawKeyboard();
  }


  drawKeyboard() {
    const c = this.c;
    c.clearRect(0, 0, this.canvas.width, this.canvas.height);
    c.strokeRect(this.margin, this.margin, this.boxW, this.boxH);
    // Draw white keys, then black keys
    for (let i = 0; i < this.countKeys; i++) {
      if (this.isWhite(i)) {
        this.drawKey(i);
      }
    }
    for (let i = 0; i <= this.countKeys; i++) {
      if (!this.isWhite(i)) {
        this.drawKey(i);
      }
    }
  }


  isWhite(i) {
    return [0,2,4,5,7,9,11].includes(i%12);
  }


  drawKey(i) {
    const c = this.c;
    let reminder = i%12;
    let k = Math.floor(i/12);
    let n = this.keyMap.slice(0, reminder).filter(x=>x==0).length;
    let x = this.margin + (k*7 + n)*this.whiteW;

    // Path
    let path = new Path2D();
    if (i in this.state && this.state[i].path != null) {
      let path = this.state[i].path;
    } else {
      this.state[i] = {
        path: path,
        isDown: false,
      };
    }

    if (this.state[i].isDown) {
      // Key pressed: colour
      c.fillStyle = '#8C00FF';
      if (this.state[i].color != null) {
        c.fillStyle = this.state[i].color;
      }
    }

    if (this.isWhite(i)) {
      path.rect(x, this.margin, this.whiteW, this.boxH);
      if (this.state[i].isDown) {
        c.fill(path);
        c.stroke(path);
      } else {
        c.stroke(path);
      }
    } else {
      // offset black keys
      x -= (1/2)*this.blackW;
      path.rect(x, this.margin, this.blackW, (3/5)*this.boxH);
      c.fill(path);
      c.stroke(path);
    }
    // Reset color
    c.fillStyle = 'black';
    c.strokeStyle = 'black';
  }


  ////// Event listeners //////
  onMouseDown(e) {
    // Get mouse coordinates
    e.preventDefault();
    if (e.button != 0) {
      return;
    }
    const state = this.state;
    const c = this.c;
    var rect = this.canvas.getBoundingClientRect();
    const mouseX = (e.clientX - rect.left)*(canvas.width/canvas.clientWidth);
    const mouseY = (e.clientY - rect.top)*(canvas.height/canvas.clientHeight);

    // If black key pressed, avoid pressing the white key next to it
    let keys = [];
    let key = null;
    for (let i in state) {
      if (state[i].hasOwnProperty('path')) {
        if (c.isPointInPath(state[i].path, mouseX, mouseY)) {
          keys.push(parseInt(i));
        }
      }
    }
    key = keys[0];
    if (keys.length > 1) {
      // TODO: Use a .filter here to simplify
      let tmp = [];
      for (let i of keys) {
        if (!this.isWhite(i)) {
          tmp.push(i);
        }
      }
      keys = tmp;
    }
    key = keys[0];

    // Send custom event
    // ref: https://developer.mozilla.org/en-US/docs/Web/Events/Creating_and_triggering_events
    if (key != null) {
      const event = new CustomEvent('keyclick', {
        // key number (starting from 0)
        detail: key,
      });
      e.target.dispatchEvent(event);
    }
  }
}; // class PianoDraw
