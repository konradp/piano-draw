// import piano-draw.js
let pd = null;

//window.onload = function() {
window.addEventListener('load', () => {
  console.log('piano-draw'); // DEBUG
  pd = new PianoDraw('canvas');
  pd.canvas.addEventListener('keyclick', (e) => {
    let color = document.querySelector('#color').value;
    let key = e.detail;
    pd.noteFlip(key, color);
    pd.drawKeyboard();
  })
  pd.setKeys([0, 4, 7]);
});
